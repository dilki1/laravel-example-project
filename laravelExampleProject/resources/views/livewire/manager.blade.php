<div>
    <x-slot name="header">
        <h2 class="text-center">Leave Manager</h2>
    </x-slot>
  
        <form>

            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <!--disaply alert when saved,update, delete manager-->
                @if (session()->has('message'))
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                    <strong class="font-bold">Message!</strong>
                    <span class="block sm:inline">{{ session('message') }}</span>
                    <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                        <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
                    </span>
                </div>
                @endif
           
                <div class="">

                    <div class="flex mb-4">
                        <div class="w-full h-12">
                            <label for="exampleFormControlInput1"
                                    class="block text-gray-700 text-sm font-bold mb-2">Name&nbsp;&nbsp;&nbsp;</label>
                                    <input type="text"
                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="fromDateInput" placeholder="Please enter from date" wire:model="name" required>
                                    @error('name') <span class="text-red-500">{{ $message }}</span>@enderror           
                        </div>
                    </div>
                   
                   
                    
                    <div class="flex -mx-2">
                            <div class="w-1/2 px-2">
                                    <label for="exampleFormControlInput1"
                                    class="block text-gray-700 text-sm font-bold mb-2">From&nbsp;&nbsp;&nbsp;</label>
                                    <input type="text"
                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="fromDateInput" placeholder="Please enter from date" wire:model="from" required>
                                    @error('from') <span class="text-red-500">{{ $message }}</span>@enderror           
                            </div>
                            <div class="w-1/2 px-2">
                                    <label for="exampleFormControlInput1"
                                    class="block text-gray-700 text-sm font-bold mb-2">To&nbsp;&nbsp;&nbsp;</label>
                                    <input type="text"
                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="toDateInput" placeholder="Please enter To date" wire:model="to" required>
                                    @error('to') <span class="text-red-500">{{ $message }}</span>@enderror           
                            </div>
                    </div>

                    <div class="flex mb-4">
                        <div class="w-full h-12">
                            <label for="exampleFormControlInput1"
                            class="block text-gray-700 text-sm font-bold mb-2">Reason&nbsp;&nbsp;&nbsp;</label>
                            <input type="text"
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="reasonInput" placeholder="Please enter reason" wire:model="reason" required>
                            @error('reason') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        
                    </div>


                </div>
            </div>
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 
            font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" wire:click.prevent="saveManager()">
                Button
            </button>


        
        </form> 
   
</div>
