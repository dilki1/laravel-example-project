<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Managers;

class Manager extends Component
{

    public $name, $to, $from, $reason, $manager_id;

    public function render()
    {
        return view('livewire.manager');
    }

    public function resetForm(){

        $this->name='';
        $this->from='';
        $this->to='';
        $this->reason='';
    }

    public function saveManager(){
        $this->validate([
            'name'=>'required|alpha',//include only letters
            'from'=>'required|date',
            'to'=>'required|date',
            'reason'=>'required'
        ]);
        
        Managers::updateOrCreate(['id'=> $this->manager_id],[
            'name' =>$this->name,
            'from' =>$this->from,
            'to' => $this->to,
            'reason' => $this->reason,
        ]);

        session()->flash('message', $this->manager_id ? 'Manager Updated Successfully!' : 'Manager Saved Successfully!');
        
        $this->resetForm();


    
    }

}
